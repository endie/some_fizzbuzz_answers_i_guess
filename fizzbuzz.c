#include <stdio.h>
#include <string.h>

#define BUZZLENGTH 5

struct Fizz {
    int multiple;
    char word[BUZZLENGTH];
};

struct Fizz buzzes[] = {
    {
        .multiple = 3,
        .word = "Fizz"
    },
    {
        .multiple = 5,
        .word = "Buzz"
    }
};

int main() {
    for (int i = 1; i <= 100; i++) {
        int buzz_amount = sizeof(buzzes) / sizeof(struct Fizz);
        char output[buzz_amount * BUZZLENGTH];

        for (int j = 0; j < buzz_amount; j++)
            if (i % buzzes[j].multiple == 0)
                strcat(output, buzzes[j].word);

        if (strlen(output) == 0)
            printf("%d\n", i);
        else {
            printf("%s\n", output);
            strcpy(output, "");
        }
    }
    return 0;
}
