fn main() {
    let buzzes = [(3, "Fizz"), (5, "Buzz")];

    for i in 1..101 {
        let output = buzzes.iter().fold(String::new(), |string, buzz| {
            if i % buzz.0 == 0 {
                string + buzz.1
            } else {
                string
            }
        });

        println!(
            "{}",
            if output.len() == 0 {
                i.to_string()
            } else {
                output
            }
        );
    }
}
