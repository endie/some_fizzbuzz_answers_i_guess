#include <iostream>
#include <utility>

std::pair<int, std::string> buzzes[] = {
    {3, "Fizz"},
    {5, "Buzz"}
};

int main() {
    for (int i = 1; i <= 100; i++) {
        std::string output;

        for (int j = 0; j < sizeof(buzzes) / sizeof(std::pair<int, std::string>); j++)
            if (i % buzzes[j].first == 0)
                output.append(buzzes[j].second);

        if (output.length() == 0)
            std::cout << i;
        else
            std::cout << output;
        std::cout << std::endl;
    }
}
