buzzes = {3: "Fizz", 5: "Buzz"}

for i in range(1, 101):
    output = ""
    for j in buzzes:
        if i % j == 0:
            output += buzzes[j]
    if len(output) == 0:
        print(i)
    else:
        print(output)
