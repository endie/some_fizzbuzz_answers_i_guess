let buzzes = [{multiple: 3, word: "Fizz"}, {multiple: 5, word: "Buzz"}]

for(i = 1; i <= 100; i++) {
    let output = ""
    buzzes.forEach(function(item) {
        if (i % item.multiple === 0)
            output += item.word
    })
    if (output.length === 0)
        print(i)
    else
        print(output)
}
